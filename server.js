const express = require('express')
const bodyParser = require ('body-parser')

const app = express()
app.use(bodyParser.json())
app.use(express.static('app'))

app.locals.contacts = [{id : 1, nume : 'test', email : '123@abc'}]

app.get('/contacts', (req, res) => {
    res.status(200).json(app.locals.contacts)
})

app.post('/contacts', (req, res) => {
    let contact = req.body
    let ids=app.locals.contacts.map((e) => e.id)
    contact.id=Math.max(...ids)+1
    app.locals.contacts.push(contact)
    res.status(201).send('created')
})

app.put('/contacts/:id', (req, res) => {
    let contact=req.body
    let index = app.locals.contacts.findIndex((e) =>
    e.id == req.params.id)
    if(index== -1){
        res.status(400).send('cannot edit non existing contact')
    } else{
        contact.id=req.params.id
        app.locals.contacts[index]=contact 
        res.status(201).send('modified')
    }
})

app.delete('/contacts/:id', (req, res) => {
    let index = app.locals.contacts.findIndex((e) =>
    e.id==req.params.id)
    if(index== -1){
        res.status(400).send('cannot delete non existing contact')
    } else{
        app.locals.contacts.splice(index, 1) 
        res.status(201).send('deleted')
    }
})

app.listen(8080)